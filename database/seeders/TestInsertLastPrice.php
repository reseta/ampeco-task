<?php

namespace Database\Seeders;

use App\Models\PriceLog;
use Illuminate\Database\Seeder;

class TestInsertLastPrice extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         PriceLog::create([
             'price' => '16930.0000',
             'pair_symbol' => 'BTCUSD',
             'price_datetime' => '2022-12-08 18:52:30',
             'created_at' => '2022-12-08 18:52:30',
             'updated_at' => '2022-12-08 18:52:30',
         ]);
    }
}
