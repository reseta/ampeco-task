<?php

use App\Enums\PricePositionEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_prices', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->decimal('watch_price', $precision = 10, $scale = 2)->index();
            $table->enum('real_price_position', PricePositionEnum::values());
            $table->string('pair_symbol', 12);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_prices');
    }
};
