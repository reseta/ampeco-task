<?php

namespace Tests\Feature;

use App\Models\User;
use Database\Seeders\TestInsertLastPrice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AddPriceWatchTest extends TestCase
{
    use RefreshDatabase;

    public function test_email_should_be_required()
    {
        $this->post('/api/track-price', ['email' => '', 'watch_price' => '16800', 'pair_symbol' => 'BTCUSD'])
            ->assertJson(['email' => ['The email field is required.']])
            ->assertStatus(400);
    }

    public function test_on_invalid_email()
    {
        $this->post('/api/track-price', ['email' => '123', 'watch_price' => '16859', 'pair_symbol' => 'BTCUSD'])
            ->assertJson(['email' => ['The email must be a valid email address.']])
            ->assertStatus(400);
    }

    public function test_on_long_email()
    {
        $this->post('/api/track-price', [
            'email' => 'qqqqqqqqqqqqqqqqwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
            wwwwqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq
            qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq@qwe.com',
            'watch_price' => '16859',
            'pair_symbol' => 'BTCUSD',
        ])
            ->assertJson(['email' => [
                'The email must be a valid email address.',
                'The email must not be greater than 255 characters.'
            ]])
            ->assertStatus(400);
    }

    public function test_last_price_missing()
    {
        $this->post('/api/track-price', ['email' => 'test@test.com', 'watch_price' => '0', 'pair_symbol' => 'BTCUSD'])
            ->assertJson(['price' => ['Missing last price.']])
            ->assertStatus(400);
    }

    public function test_success_insertion()
    {
        $this->seed(TestInsertLastPrice::class);

        $this->post('/api/track-price', ['email' => 'test@test.com', 'watch_price' => '16930', 'pair_symbol' => 'BTCUSD'])
            ->assertJson(['status' => 'Success', 'code' => 200,])
            ->assertStatus(200);

        $this->assertDatabaseCount('track_prices', 1);
        $this->assertDatabaseHas('track_prices', [
            'email' => 'test@test.com',
            'watch_price' => '16930',
            'pair_symbol' => 'BTCUSD',
        ]);
    }
}
