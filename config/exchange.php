<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the config for queues
    |
    */

    'broadcast_to_channel' => 'exchange',
    'cache_prefix' => 'exchange',
    'default' => [
        'response_timeout' => 15,
        'timeout' => 15,
        'tries' => 3,
        'backoff' => 3,
        'task_scheduling_cron' => '0 * * * *',
        'endpoint' => 'https://api.bitfinex.com/v1/pubticker/BTCUSD',
    ],
    'active_pairs' => [
        'BTCUSD' => [
            'task_scheduling_cron' => '*/2 * * * *',
        ],
    ],
];
