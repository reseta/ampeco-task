<?php

namespace App\Listeners;

use App\Enums\PricePositionEnum;
use App\Events\PriceChangedEvent;
use App\Mail\SendPriceNotificationMail;
use App\Models\TrackPrice;
use DateTime;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendPriceNotificationListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * The number of times the queued listener may be attempted.
     *
     * @var int
     */
    public int $tries = 3;

    /**
     * Determine the time at which the listener should time out.
     *
     * @return DateTime
     */
    public function retryUntil(): DateTime
    {
        return now()->addMinute();
    }

    /**
     * Dispatch after DB commit
     *
     * @var bool
     */
    public bool $afterCommit = true;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PriceChangedEvent  $event
     * @return void
     */
    public function handle(PriceChangedEvent $event)
    {
        $priceLog = $event->priceLog;

        // get emails for notification
        $notifyEmails = TrackPrice::getNotifyEmails($priceLog);

        foreach ($notifyEmails as $item) {
            Mail::to($item->email)->send(new SendPriceNotificationMail($priceLog, $item));
        }

        // delete sent notifications
        TrackPrice::whereIn('id', $notifyEmails->pluck('id'))->delete();
    }
}

