<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceLog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string, string>
     */
    protected $fillable = ['price', 'pair_symbol', 'price_datetime'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'price' => 'float',
        'pair_symbol' => 'string',
        'price_datetime' => 'datetime',
    ];

    public static function getChartData(array $data = [])
    {
        // set default dates if not provided
        $date_from = $data['date_from'] ?? now()->subDays(7);
        $date_to = $data['date_to'] ?? now();

        return static::select('price', 'price_datetime')
            ->whereDate('price_datetime', '>=', $date_from->format('Y-m-d H:m:s'))
            ->whereDate('price_datetime', '<=', $date_to->format('Y-m-d H:m:s'))
            ->get()
            ->toJson();
    }
}
