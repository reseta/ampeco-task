<?php

namespace App\Models;

use App\Enums\PricePositionEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrackPrice extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string, string>
     */
    protected $fillable = ['email', 'watch_price', 'pair_symbol'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email' => 'string',
        'watch_price' => 'float',
        'pair_symbol' => 'string',
    ];

    public static function getNotifyEmails(PriceLog $priceLog)
    {
        // get emails for notification
        return self::select('id', 'email', 'watch_price')
            ->where('watch_price', '<=', $priceLog->price)
            ->where(function ($query) use ($priceLog) {
                $query->where('real_price_position', '=', PricePositionEnum::LOW)
                    ->where('pair_symbol', '=', $priceLog->pair_symbol);
            })
            ->orWhere('watch_price', '>=', $priceLog->price)
            ->where(function ($query) use ($priceLog) {
                $query->where('real_price_position', '=', PricePositionEnum::HIGH)
                    ->where('pair_symbol', '=', $priceLog->pair_symbol);
            })
            ->get();
    }
}
