<?php

namespace App\Providers;

use App\AppHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $cachePrefix = config('exchange.cache_prefix');
        if ($cachePrefix && !Cache::has("{$cachePrefix}:lifetime")) {
            AppHelper::redisQueueSettings();
        }
    }
}
