<?php

namespace App\Jobs;

use App\Events\PriceChangedEvent;
use App\Models\PriceLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class ProcessPriceJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var array
     */
    public array $settings;

    /**
     * The number of seconds after which the job's unique lock will be released.
     *
     * @var int
     */
    public int $uniqueFor = 120;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public int $tries;

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public int $backoff;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $cachePrefix = '')
    {
        $settings = Cache::get($cachePrefix, []);
        if (empty($settings)) {
            $this->fail();
        }

        $this->settings = $settings;
        $this->tries = $settings['tries'];
        $this->backoff = $settings['backoff'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Http request
        $response = Http::timeout($this->settings['response_timeout'])
            ->get($this->settings['endpoint']);

        // fail or retry on error
        if ($response->clientError()) {
            $this->fail();
        } elseif ($response->serverError()) {
            $this->release();
        }

        // Save to db
        $result = json_decode($response->body());
        $priceLog = PriceLog::create([
            'price' => $result->last_price,
            'pair_symbol' => $this->settings['pair_symbol'],
            'price_datetime' => $result->timestamp,
        ]);
        $priceLog->save();

        // Dispatch event
        PriceChangedEvent::dispatch($priceLog);
    }
}
