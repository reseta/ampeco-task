<?php

namespace App\Console;

use App\Jobs\ProcessPriceJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $exchangeCachePrefix = config('exchange.cache_prefix');

        // Run job for active pairs
        foreach (array_keys(config('exchange.active_pairs', [])) as $pair) {
            $exchangeConfig = Cache::get("$exchangeCachePrefix:$pair");

            $schedule->job(new ProcessPriceJob("$exchangeCachePrefix:$pair"))
                ->cron($exchangeConfig['task_scheduling_cron'] ?? '0 * * * *')
                ->onOneServer();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
