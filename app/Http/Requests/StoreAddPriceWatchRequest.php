<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class StoreAddPriceWatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function rules(): array
    {
        $email = request()->get('email');
        $watchPrice = request()->get('watch_price');

        return [
            'email' => [
                'required',
                'email:rfc,dns',
                'max:255',
                Rule::unique('track_prices')->where(function ($query) use ($email, $watchPrice) {
                    return $query->where('email', $email)
                        ->where('watch_price', $watchPrice);
                }),
            ],
            'watch_price' => ['required', 'numeric', 'between:0,9999999999.99'],
            'pair_symbol' => ['required', 'string', 'min:3', 'max:12'],
        ];
    }

    /**
     * Custom validation messages
     *
     * @return string[]
     */
    public static function getMessages(): array
    {
        return [
            'email.unique' => 'Alert for this price and e-mail already exist',
        ];
    }
}
