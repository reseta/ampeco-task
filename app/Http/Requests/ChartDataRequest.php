<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChartDataRequest extends FormRequest
{
    /**
     * Validation rules for store price watch
     *
     * @return array<string, mixed>
     */
    public static function rules(): array
    {
        return [
            'pair_symbol' => ['required', 'string', 'min:3', 'max:12'],
            'date_from' => ['date'],
            'date_to' => ['date'],
        ];
    }
}
