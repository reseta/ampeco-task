<?php

namespace App\Http\Controllers;

use App\Enums\PricePositionEnum;
use App\Http\Requests\StoreAddPriceWatchRequest;
use App\Models\PriceLog;
use App\Models\TrackPrice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;

class AddPriceWatchController extends Controller
{
    /**
     * Store a new price tracking.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            StoreAddPriceWatchRequest::rules(),
            StoreAddPriceWatchRequest::getMessages()
        );

        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        // Get last actual price from db
        $actualPrice = PriceLog::latest()->first();
        if (!$actualPrice) {
            return response()->json(['price' => ['Missing last price.']], Response::HTTP_BAD_REQUEST);
        }

        // Save to DB
        $priceWatch = new TrackPrice($validator->validated());
        $priceWatch->real_price_position =
            ($actualPrice->price > $request->get('watch_price')) ? PricePositionEnum::HIGH : PricePositionEnum::LOW;
        $priceWatch->save();

        return $this->jsonResponseTransform(Response::HTTP_OK);
    }
}
