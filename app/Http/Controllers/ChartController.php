<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChartDataRequest;
use App\Models\PriceLog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ChartController extends Controller
{
    /**
     * Show the chart and form to create a new price watch record.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function data(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), ChartDataRequest::rules());

        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        // get data from db
        $data = PriceLog::getChartData($validator->validated());

        return $this->jsonResponseTransform(Response::HTTP_OK, json_encode($data));
    }
}
