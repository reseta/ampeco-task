<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function jsonResponseTransform(int $statusCode = 200, $data = []): JsonResponse
    {
        return match ($statusCode) {
            400 => response()->json(
                ['status' => 'Fail', 'code' => Response::HTTP_BAD_REQUEST],
                Response::HTTP_BAD_REQUEST,
            ),
            default => response()->json(
                [
                    'status' => 'Success',
                    'code' => Response::HTTP_OK,
                    'data' => $data,
                ],
                Response::HTTP_OK,
            ),
        };
    }
}
