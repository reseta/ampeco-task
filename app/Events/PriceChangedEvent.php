<?php

namespace App\Events;

use App\Models\PriceLog;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PriceChangedEvent implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * The PriceLog instance.
     *
     * @var PriceLog
     */
    public PriceLog $priceLog;

    /**
     * Create a new event instance.
     *
     * @param  PriceLog $priceLog
     * @return void
     */
    public function __construct(PriceLog $priceLog)
    {
        $this->priceLog = $priceLog;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        return new PrivateChannel(config('exchange.broadcast_to_channel'));
    }
}
