<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class AppHelper
{
    /**
     * Setup config settings to cache
     *
     * @return void
     */
    public static function redisQueueSettings(): void
    {
        $exchangeCachePrefix = config('exchange.cache_prefix');

        // Build all pairs and set config to cache
        foreach (config('exchange.active_pairs') as $pair_symbol => $pair) {
            $cacheObj['pair_symbol'] = $pair_symbol;
            foreach (array_keys(config('exchange.default')) as $key) {
                $cacheObj[$key] = config("exchange.active_pairs.{$pair_symbol}.{$key}") ?? config("exchange.default.{$key}");
            }

            Cache::forever("{$exchangeCachePrefix}:{$pair_symbol}", $cacheObj);
        }

        // Setup cache to be updated daily
        Cache::put("{$exchangeCachePrefix}:lifetime", true, now()->addDay());
    }
}
