<?php

namespace App\Enums;

use App\Traits\EnumToArray;

enum PricePositionEnum: string
{
    use EnumToArray;

    case LOW = 'low';
    case HIGH = 'high';
}
