<?php

namespace App\Mail;

use App\Models\PriceLog;
use App\Models\TrackPrice;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SendPriceNotificationMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * The PriceLog instance.
     *
     * @var PriceLog
     */
    protected PriceLog $priceLog;

    /**
     * The TrackPrice instance.
     *
     * @var TrackPrice
     */
    protected TrackPrice $trackPrice;

    /**
     * Create a new message instance.
     *
     * @param  PriceLog $priceLog
     * @return void
     */
    public function __construct(PriceLog $priceLog, TrackPrice $trackPrice)
    {
        $this->priceLog = $priceLog;
        $this->trackPrice = $trackPrice;
    }

    /**
     * Get the message envelope.
     *
     * @return Envelope
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Send Mail Price Notification',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return Content
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.price-notification',
            with: [
                'priceLog' => $this->priceLog,
                'trackPrice' => $this->trackPrice,
            ],
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments(): array
    {
        return [];
    }
}
